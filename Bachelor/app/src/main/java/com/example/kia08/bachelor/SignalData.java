package com.example.kia08.bachelor;

/**
 * Created by kia08 on 3/22/2018.
 */

class SignalData {
private double leftEar;
private double rightEar;

    public double getLeftEar() {
        return leftEar;
    }

    public void setLeftEar(double leftEar) {
        this.leftEar = leftEar;
    }

    public double getRightEar() {
        return rightEar;
    }

    public void setRightEar(double rightEar) {
        this.rightEar = rightEar;
    }
}
